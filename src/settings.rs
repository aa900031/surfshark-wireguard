use crate::wireguard::key_pair::gen_private_key;
use std::env;
use std::io::prelude::*;
use std::fs::File;
use config::Value;
use config::{Config, ConfigBuilder, builder::DefaultState};
use serde::Deserialize;
use anyhow::{anyhow, Result};

#[derive(Debug, Deserialize, Clone)]
pub struct SettingData {
	pub auth_token: String,
	pub auth_renew_token: String,
	pub private_key: String,
}

pub struct Settings {
	path: String,
	pub data: SettingData,
}

impl Settings {
	pub fn new() -> Settings {
		let path = get_config_path().unwrap();
		let builder = create_builder(&path).unwrap();
		let data = builder
			.build_cloned().unwrap()
			.try_deserialize::<SettingData>().unwrap();

		Settings {
			path,
			data,
		}
	}

	pub fn update<T>(
		&mut self,
		key: &str,
		value: T,
	) -> Result<()> where
		T: Into<Value>,
	{
		let builder = create_builder(&self.path)?
			.set_override(key, value)?;

		let data = &builder
			.build_cloned()?
			.try_deserialize::<SettingData>()?;

		let value = &builder
			.build_cloned()?
			.try_deserialize::<serde_yaml::Value>()?;

		let content = serde_yaml::to_string(&value)?;
		writer(&self.path, content.as_bytes())?;

		self.data = data.clone();

		Ok(())
	}
}

fn create_builder(path: &str) -> Result<ConfigBuilder<DefaultState>>{
	let result = Config::builder()
		.add_source(config::File::with_name(&path))
		.set_default("private_key", gen_private_key()).unwrap();

	Ok(result)
}

fn get_config_path() -> Result<String> {
	let mut path = env::current_dir()?;
	path = path.join("Config.yaml");

	match path.as_path().to_str().map(|s| s.to_string()) {
		Some(val) => Ok(val),
		None => Err(anyhow!("")),
	}
}

fn writer(file_name: &str, output_data: &[u8]) -> Result<()> {
	let mut file = File::create(file_name)?;
	file.write_all(output_data)?;
	Ok(())
}
