use crate::SETTINGS;
use crate::surfshark_client::{set_wireguard_public_key, SurfsharkClient};
use crate::wireguard::key_pair::{gen_public_key};
use std::sync::Arc;
use anyhow::Result;
use clap::{Command};

pub const CLI_APP_NAME: &str = "register";

pub fn create_app() -> Command<'static> {
	Command::new(CLI_APP_NAME)
}

pub async fn exec() -> Result<()> {
	let settings = Arc::clone(&SETTINGS);
	let settings = settings.lock().unwrap();
	let public_key = gen_public_key(&settings.data.private_key)?;
	drop(settings);

	let mut client = SurfsharkClient::new();
	let register = set_wireguard_public_key(&mut client, &public_key).await?;

	let settings = Arc::clone(&SETTINGS);
	let settings = settings.lock().unwrap();
	println!("private_key: {}", &settings.data.private_key);
	println!("expires_at:  {}", &register.expires_at);
	drop(settings);

	Ok(())
}
