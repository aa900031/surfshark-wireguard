extern crate clap;
extern crate tokio;

mod register;
mod servers;
mod settings;
mod surfshark_client;
mod wireguard;

use anyhow::Result;
use clap::Command;
use lazy_static::lazy_static;
use settings::Settings;
use std::sync::{Arc, Mutex};

lazy_static! {
	static ref SETTINGS: Arc<Mutex<Settings>> = Arc::new(Mutex::new(Settings::new()));
}

#[tokio::main]
async fn main() -> Result<()> {
	let app = Command::new(env!("CARGO_PKG_NAME"))
		.version(env!("CARGO_PKG_VERSION"))
		.about(env!("CARGO_PKG_DESCRIPTION"))
		.author("zhong666")
		.subcommand(register::cli::create_app())
		.subcommand(servers::cli::create_app());

	match app.get_matches().subcommand() {
		Some(("register", _matches)) => register::cli::exec().await?,
		Some(("servers", _matches)) => servers::cli::exec().await?,
		_ => unreachable!("parser should ensure only valid subcommand names are used"),
	}

	Ok(())
}
