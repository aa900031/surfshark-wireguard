use super::BASE_URL;
use super::USER_AGENT_VAL;
use anyhow::Result;
use reqwest::header::{ACCEPT, AUTHORIZATION, CONTENT_TYPE, USER_AGENT};
use reqwest::{Client, Url};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct RegisterPublicKeyResult {
	#[serde(rename = "expiresAt")]
	pub expires_at: String,

	#[serde(rename = "pubKey")]
	pub pub_key: String,

	#[serde(rename = "id")]
	pub id: String,
}

pub async fn exec(
	client: &Client,
	token: &String,
	pub_key: &String,
) -> Result<RegisterPublicKeyResult> {
	let url = Url::parse(BASE_URL)?.join("/v1/account/users/public-keys")?;

	let mut params = HashMap::new();
	params.insert("pubKey", pub_key);

	let response = client
		.post(url)
		.header(ACCEPT, "application/json")
		.header(AUTHORIZATION, format!("Bearer {}", token))
		.header(CONTENT_TYPE, "application/json")
		.header(USER_AGENT, USER_AGENT_VAL)
		.json(&params)
		.send()
		.await?
		.error_for_status()?;

	let data = response.text().await?;
	// dbg!(&data);

	let json = serde_json::from_str::<RegisterPublicKeyResult>(&data)?;

	Ok(json)
}
