use super::BASE_URL;
use super::USER_AGENT_VAL;
use anyhow::Result;
use reqwest::header::{ACCEPT, AUTHORIZATION, USER_AGENT};
use reqwest::{Client, Url};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AuthToken {
	#[serde(rename = "token")]
	pub token: String,

	#[serde(rename = "renewToken")]
	pub renew_token: String,
}

pub async fn exec(client: &Client, renew_token: &String) -> Result<AuthToken> {
	let url = Url::parse(BASE_URL)?.join("/v1/auth/renew")?;
	let response = client
		.post(url)
		.header(ACCEPT, "application/json")
		.header(AUTHORIZATION, format!("Bearer {}", renew_token))
		.header(USER_AGENT, USER_AGENT_VAL)
		.send()
		.await?
		.error_for_status()?;

	let data = response.text().await?;
	// dbg!(&data);

	let json = serde_json::from_str::<AuthToken>(&data)?;

	Ok(json)
}
