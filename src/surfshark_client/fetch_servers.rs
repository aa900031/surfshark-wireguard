use super::BASE_URL;
use super::USER_AGENT_VAL;
use anyhow::Result;
use reqwest::header::{ACCEPT, AUTHORIZATION, USER_AGENT};
use reqwest::{Client, Url};
use serde::{Deserialize, Serialize};

pub type ServerList = Vec<ServerListElement>;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ServerListElement {
	#[serde(rename = "country")]
	country: String,

	#[serde(rename = "countryCode")]
	country_code: String,

	#[serde(rename = "region")]
	region: Region,

	#[serde(rename = "regionCode")]
	region_code: RegionCode,

	#[serde(rename = "load")]
	load: i64,

	#[serde(rename = "id")]
	id: String,

	#[serde(rename = "coordinates")]
	coordinates: Coordinates,

	#[serde(rename = "info")]
	info: Option<Vec<Info>>,

	#[serde(rename = "type")]
	server_list_type: Type,

	#[serde(rename = "location")]
	location: String,

	#[serde(rename = "connectionName")]
	pub connection_name: String,

	#[serde(rename = "pubKey")]
	pub pub_key: String,

	#[serde(rename = "tags")]
	pub tags: Vec<ServerTag>,

	#[serde(rename = "transitCluster")]
	transit_cluster: Option<serde_json::Value>,

	#[serde(rename = "flagUrl")]
	flag_url: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Coordinates {
	#[serde(rename = "longitude")]
	longitude: f64,

	#[serde(rename = "latitude")]
	latitude: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Info {
	#[serde(rename = "id")]
	id: String,

	#[serde(rename = "entry")]
	entry: Entry,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Entry {
	#[serde(rename = "value")]
	value: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Region {
	#[serde(rename = "Asia Pacific")]
	AsiaPacific,

	#[serde(rename = "Europe")]
	Europe,

	#[serde(rename = "Middle East and Africa")]
	MiddleEastAndAfrica,

	#[serde(rename = "The Americas")]
	TheAmericas,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum RegionCode {
	#[serde(rename = "AM")]
	Am,

	#[serde(rename = "AP")]
	Ap,

	#[serde(rename = "EA")]
	Ea,

	#[serde(rename = "EU")]
	Eu,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Type {
	#[serde(rename = "generic")]
	Generic,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum ServerTag {
	#[serde(rename = "p2p")]
	P2P,

	#[serde(rename = "physical")]
	Physical,

	#[serde(rename = "virtual")]
	Virtual,
}

pub async fn exec(client: &Client, token: &String) -> Result<ServerList> {
	let url = Url::parse(BASE_URL)?.join("/v4/server/clusters/generic?countryCode=")?;
	let response = client
		.get(url)
		.header(ACCEPT, "application/json")
		.header(AUTHORIZATION, format!("Bearer {}", token))
		.header(USER_AGENT, USER_AGENT_VAL)
		.send()
		.await?
		.error_for_status()?;

	let data = response.text().await?;
	// dbg!(&data);

	let json = serde_json::from_str::<ServerList>(&data)?;

	Ok(json)
}
