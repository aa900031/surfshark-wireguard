pub mod auth_renew;
pub mod fetch_servers;
pub mod register_pub_key;
pub mod validate_pub_key;

use crate::SETTINGS;
use std::sync::Arc;
use anyhow::Result;
use fetch_servers::{ServerList, ServerListElement, ServerTag};
use register_pub_key::RegisterPublicKeyResult;
use reqwest::{Client, StatusCode};

const BASE_URL: &str = "https://api.surfshark.com";

const USER_AGENT_VAL: &str = "Surfshark/2.25.2 (com.surfshark.vpnclient.ios; build:4; iOS 15.2.1) Alamofire/5.4.3 device/mobile";

const WIREGUARD_PORT: &str = "51820";

pub struct SurfsharkClient {
	client: Client,
}

impl SurfsharkClient {
	pub fn new() -> Self {
		SurfsharkClient {
			client: Client::new(),
		}
	}

	pub async fn fetch_servers(&mut self) -> Result<ServerList> {
		let token = get_token();

		match fetch_servers::exec(&self.client, &token).await {
			Ok(val) => Ok(val),
			Err(e) => {
				if let Some(ierr) = e.downcast_ref::<reqwest::Error>() {
					if let Some(status) = ierr.status() {
						if status == StatusCode::UNAUTHORIZED {
							self.auth_renew().await?;

							let token = get_token();
							return Ok(fetch_servers::exec(&self.client, &token).await?)
						}
					}
				}
				Err(e)
			}
		}
	}

	pub async fn register_pub_key(&mut self, pub_key: &String) -> Result<RegisterPublicKeyResult> {
		let token = get_token();

		match register_pub_key::exec(&self.client, &token, &pub_key).await {
			Ok(val) => Ok(val),
			Err(e) => {
				if let Some(ierr) = e.downcast_ref::<reqwest::Error>() {
					if let Some(status) = ierr.status() {
						if status == StatusCode::UNAUTHORIZED {
							self.auth_renew().await?;

							let token = get_token();
							return Ok(register_pub_key::exec(&self.client, &token, &pub_key).await?)
						}
					}
				}
				Err(e)
			}
		}
	}

	pub async fn validate_pub_key(&mut self, pub_key: &String) -> Result<RegisterPublicKeyResult> {
		let token = get_token();

		match validate_pub_key::exec(&self.client, &token, &pub_key).await {
			Ok(val) => Ok(val),
			Err(e) => {
				if let Some(ierr) = e.downcast_ref::<reqwest::Error>() {
					if let Some(status) = ierr.status() {
						if status == StatusCode::UNAUTHORIZED {
							self.auth_renew().await?;

							let token = get_token();
							return Ok(validate_pub_key::exec(&self.client, &token, &pub_key).await?);
						}
					}
				}
				Err(e)
			}
		}
	}

	async fn auth_renew(&mut self) -> Result<()> {
		let settings = Arc::clone(&SETTINGS);
		let mut settings = settings.lock().unwrap();

		let response = auth_renew::exec(&self.client, &settings.data.auth_renew_token).await?;

		settings.update("auth_token", response.token)?;
		settings.update("auth_renew_token", response.renew_token)?;

		Ok(())
	}
}

fn get_token() -> String {
	let settings = Arc::clone(&SETTINGS);
	let settings = settings.lock().unwrap();
	let result = settings.data.auth_token.clone();
	result
}

pub fn get_server_by_tags(servers: &ServerList, tags: Vec<ServerTag>) -> ServerList {
	servers
		.iter()
		.filter(|item| item.tags.iter().any(|tag| tags.iter().any(|t| t == tag)))
		.map(|item| item.clone())
		.collect::<ServerList>()
}

pub fn get_wireguard_endpoint(server: &ServerListElement) -> String {
	format!("{}:{}", &server.connection_name, &WIREGUARD_PORT)
}

pub async fn set_wireguard_public_key(
	client: &mut SurfsharkClient,
	public_key: &String,
) -> Result<RegisterPublicKeyResult> {
	match client.register_pub_key(&public_key).await {
		Ok(val) => Ok(val),
		Err(e) => {
			if let Some(ierr) = e.downcast_ref::<reqwest::Error>() {
				if let Some(status) = ierr.status() {
					if status == StatusCode::UNAUTHORIZED {
						return Err(e);
					}
				}
			}

			Ok(client.validate_pub_key(&public_key).await?)
		}
	}
}
