use base64;
use anyhow::Result;
use rand_core::OsRng;
use x25519_dalek::{PublicKey, StaticSecret};
use serde::{Serialize, Deserialize};

const KEY_SIZE: usize = 32;

// fn b64_encode<'a>(input: &[u8], out: &'a mut [u8]) -> &'a mut [u8] {
// 	let written = base64::encode_config_slice(input, base64::STANDARD, out);
// 	&mut out[..written]
// }

fn b64_decode(input: &[u8], out: &mut [u8]) -> Result<usize, base64::DecodeError> {
	base64::decode_config_slice(input, base64::STANDARD, out)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct KeyPair {
	pub public: String,
	pub private: String,
}

pub fn gen_public_key(
	private_key: &String,
) -> Result<String> {
	let mut private = [0; KEY_SIZE];
	let _ = b64_decode(private_key.as_bytes(), &mut private)?;
	let private = StaticSecret::from(private);
	let public = PublicKey::from(&private);
	let public_b64 = base64::encode(&public.as_bytes());
	Ok(public_b64)
}

pub fn gen_private_key() -> String {
	let private = StaticSecret::new(&mut OsRng);
	let private_b64 = base64::encode(&private.to_bytes());
	private_b64
}
