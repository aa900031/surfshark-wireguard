use crate::surfshark_client::{
	fetch_servers::{ServerList, ServerTag},
	get_server_by_tags, get_wireguard_endpoint, SurfsharkClient,
};
use anyhow::Result;
use clap::{Command};
use comfy_table::Table;

pub const CLI_APP_NAME: &str = "servers";

fn print_servers(servers: &ServerList) {
	let mut table = Table::new();
	table.set_header(vec!["Address", "Public Key"]);

	for server in servers {
		table.add_row(vec![
			get_wireguard_endpoint(&server),
			server.pub_key.to_string(),
		]);
	}

	println!("{}", table);
}

pub fn create_app() -> Command<'static> {
	Command::new(CLI_APP_NAME)
}

pub async fn exec() -> Result<()> {
	let mut client = SurfsharkClient::new();

	let servers = client.fetch_servers().await?;
	let servers = get_server_by_tags(&servers, vec![ServerTag::P2P, ServerTag::Physical]);
	print_servers(&servers);

	Ok(())
}
