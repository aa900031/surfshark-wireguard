# surfshark_wireguard

## Start
copy `Config.example.yaml` and rename `Config.yaml`
```
cp Config.example.yaml Config.yaml
```

open `Config.yaml` and edit
P.S Not use command in this file, because it will be override
- `auth_token`: Surfshark auth JWT, can get in Browser cookie name with `_sstk`
- `auth_renew_token`: Surfshark renew auth JWT, can get in Browser cookie name with `_ssrtk`
- `private_key`: Wireguard private key encode with base64

## Command "register"
Register the self Wireguard public key to Surfshark

```
surfshark-wireguard register
```

## Command "servers"
List Surfshark servers of support Wireguard

```
surfshark-wireguard servers
```
